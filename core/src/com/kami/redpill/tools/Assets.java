package com.kami.redpill.tools;

import com.badlogic.gdx.assets.AssetManager;

/**
 * This class handles all the loading and disposing the assets
 * @author Kami Nasri.
 */
public class Assets {
    public static final AssetManager manager = new AssetManager();

    /**
     * Loads all the assets
     */
    public static void load(){

    }

    /**
     * Disposes all the assets
     */
    public static void dispose(){
        manager.dispose();
    }
}
